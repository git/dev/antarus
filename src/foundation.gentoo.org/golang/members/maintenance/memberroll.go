package maintenance

import (
	"fmt"
	"io/ioutil"
	"time"

	pb "foundation.gentoo.org/golang/members/data"
	"github.com/golang/protobuf/proto"
)

// Struct MemberRoll defines operations on a MemberRoll
// and holds the underlying data.
type MemberRoll struct {
	roll *pb.MemberRoll
}

// Create a new memberRoll from a file.
func NewMemberRoll(path string) (*MemberRoll, error) {
	in, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
		// something
	}
	r := &pb.MemberRoll{}
	if err = proto.Unmarshal(in, r); err != nil {
		return nil, err
		// something
	}
	mr := &MemberRoll{roll: r}
	return mr, nil
}

// NewEmptyMemberRoll creates an empty MemberRoll
func NewEmptyMemberRoll() *MemberRoll {
	// The first member gets ID 1.
	return &MemberRoll{
		roll: &pb.MemberRoll{
			NextMemberId: proto.Int64(1),
		},
	}
}

// Save will write the MemberRoll to file at path.
func (mr *MemberRoll) Save(path string) (bool, error) {
	bytes, err := proto.Marshal(mr.roll)
	if err != nil {
		return false, err
	}
	if err = ioutil.WriteFile(path, bytes, 0644); err != nil {
		return false, err
	}
	return true, nil
}

// Does this email match an existing member?
func (mr *MemberRoll) MatchEmail(email string) *pb.Member {
	member := &pb.Member{
		Email: []string{email},
	}
	return mr.Match(member)
}

// Members match if they shared an email address.
func Match(m1, m2 *pb.Member) bool {
	for _, i := range m1.GetEmail() {
		for _, j := range m2.GetEmail() {
			if i == j {
				return true
			}
		}
	}
	return false
}

// Match will try to find if m is in mr.
func (mr *MemberRoll) Match(m *pb.Member) *pb.Member {
	for _, i := range mr.roll.GetMembers() {
		if Match(m, i) {
			return i
		}
	}
	return nil
}

// low-level function for add raw member protobufs to the roll.
func (mr *MemberRoll) Add(m pb.Member) (bool, error) {
	match := mr.Match(&m)
	if match != nil {
		err := fmt.Errorf("Adding existing member: %v", match)
		return false, err
	}
	mr.roll.Members = append(mr.roll.Members, &m)
	return true, nil
}

func (mr *MemberRoll) RecordVote(id int64, t time.Time) error {
	return nil
}

func (mr *MemberRoll) Print() {
	for _, member := range mr.roll.GetMembers() {
		fmt.Printf("Member: %v\n", member)
	}
}

// GetMaxMemberId computes the largest ID amongst a memberroll.
// This works by assuming members are not removed from a roll.
func (mr *MemberRoll) GetMaxMemberId() int64 {
	var id int64 = 0
	for _, member := range mr.roll.GetMembers() {
		member_id := member.GetId()
		if member_id > id {
			id = member_id
		}
	}
	return id
}

func (mr *MemberRoll) GetNextMemberId() int64 {
	id := mr.GetNextMemberId()
	// If the member roll lost its ID (not a required field.)
	// Recalculate based on existing membership.
	if id == 0 {
		id = mr.GetMaxMemberId()
	}
	return id
}
