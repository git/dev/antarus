resource "google_compute_instance_template" "rsync-node-template" {
  name        = "rsync-node-template"
  description = "This template is used to create rsync node instances."

  tags = ["rsync"]

  labels = {
		container-vm = "cos-stable-71-11151-60-0"
    environment = "prod"
  }

  instance_description = "rsync node"
  machine_type         = "n1-standard-1"
  can_ip_forward       = false

  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  disk {
    source_image = "projects/cos-cloud/global/images/cos-stable-71-11151-60-0"
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network = "default"
		access_config {
			network_tier = "STANDARD"
		}
	}

  metadata {
		google-logging-enabled = "true"
		gce-container-declaration = "spec:\n  containers:\n    - name: rsync-4\n      image: us.gcr.io/gentoo-infra-dev/rsync-node:prod\n      securityContext:\n        privileged: true\n      stdin: false\n      tty: false\n  restartPolicy: Always\n\n# This container declaration format is not public API and may change without notice. Please\n# use gcloud command-line tool or Google Cloud Console to run Containers on Google Compute Engine."
  }

  service_account {
		scopes = [
          "https://www.googleapis.com/auth/devstorage.read_only",
          "https://www.googleapis.com/auth/logging.write",
          "https://www.googleapis.com/auth/monitoring.write",
          "https://www.googleapis.com/auth/servicecontrol",
          "https://www.googleapis.com/auth/service.management.readonly",
          "https://www.googleapis.com/auth/trace.append"
	  ]
  }
}

resource "google_compute_region_instance_group_manager" "rsync-node-mig" {
  name = "rsync-node-mig"

  base_instance_name = "rsync-node"
  instance_template  = "${google_compute_instance_template.rsync-node-template.self_link}"
  update_strategy    = "NONE"
  region             = "us-central1"
  target_size        = 1
  target_pools       = ["${google_compute_target_pool.rsync-in2.self_link}"]
}

resource "google_compute_region_autoscaler" "rsync-autoscaler" {
	name = "rsync-autoscaler"
	target = "${google_compute_region_instance_group_manager.rsync-node-mig.self_link}"
	autoscaling_policy {
		min_replicas = 1
		max_replicas = 3
		cooldown_period = 60
		cpu_utilization {
			target = 0.90
		}
	}
}

resource "google_compute_target_pool" "rsync-in2" {
  name = "rsync-in2"
}

//data "google_compute_forwarding_rule" "rsync-dev" {
//	name = "rsync-dev"
//	ip_address = "35.190.132.250"
//	ports = ["873"]
//}

resource "google_compute_firewall" "rsync-in" {
	name = "rsync-in"
	network = "${google_compute_network.default.self_link}"
	allow {
		protocol = "tcp"
		ports = ["873"]
	}
	target_tags = ["rsync"]
}

resource "google_compute_network" "default" {
	name = "default"
	description = "Default network for the project"
}
