#!/bin/bash

# curl -q -s -F "content=$( bugz -q --key "${APIKEY}" search ""${PACKAGE}" fails test" )" http://dpaste.com/api/v2/ ).txt"

die() {
	echo "${1}"
	exit 1
}

env_update() {
	env-update > /dev/null 2>&1
	source /etc/profile > /dev/null 2>&1
}

update_world() {
	echo -ne "" > /etc/portage/package.use/test
	echo -ne "" > /etc/portage/package.env
	FEATURES="-collision-protect" emerge -DuNq world > /dev/null 2>&1 || die "emerge -DuNq world failed"
	emerge -cq > /dev/null 2>&1 || die "depclean failed"

	if [ -n "$( portageq list_preserved_libs / )" ]
	then
		emerge -q1 @preserved-rebuild > /dev/null 2>&1 || die "preserved-rebuild failed"
	fi

	rm -fr /var/log/emerge.log
}

native-symlinks() {
	echo "sys-devel/gcc-config -native-symlinks" >> /etc/portage/package.use/test
	echo "sys-devel/binutils-config -native-symlinks" >> /etc/portage/package.use/test
	echo "dev-lang/python-exec -native-symlinks" >> /etc/portage/package.use/test
	emerge -q1 sys-devel/gcc-config sys-devel/binutils-config dev-lang/python-exec > /dev/null 2>&1 || die "native-symlinks failed"
	env_update
}

common_merge() {
	SLOTS="$( V='{slotfirst}<slot>\n{}' eix --format '<availableversions::V>' -e "${PACKAGE}" )"
	unset STATUS

	if [ "${SLOTS}" = "No matches found" ]
	then
		SLOTS="0"
	fi

	for SLOT in ${SLOTS}
	do
		rm -fr /var/tmp/portage /root/.packages/"${PACKAGE}"* /root/.packages/dev-python/pandas*

		START_MERGE="$( date +%s )"
		echo "$( date '+%d-%m-%Y %H:%M' ) START ${PACKAGE}:${SLOT} ${MERGETYPE}"
		emerge "${PACKAGE}":"${SLOT}" ${EOPTS} > /dev/null 2>&1
		if [ "${?}" != "0" ] && [ -n "$( find /var/tmp/portage -type l -name "build.log" 2>/dev/null )" ]
		then
			STATUS="fail"
			SHUF="$( shuf -i0000000000000000000-9999999999999999999 -n1 )"
			mkdir -p /root/.failures

			if [ "$( find /var/tmp/portage -type l -name "build.log" | wc -l )" -gt "1" ]
			then
				for FILE in $( find /var/tmp/portage -type l -name "build.log" )
				do
					SHUF="$( shuf -i0000000000000000000-9999999999999999999 -n1 )"
					cp "${FILE}" /root/.failures/"${SHUF}"_main
				done
			elif [ "$( find /var/tmp/portage -type l -name "build.log" | wc -l )" = "1" ]
			then
				cp "$( find /var/tmp/portage -type l -name "build.log" )" /root/.failures/"${SHUF}"_main

				for LOG in ${COMMON_ERROR_LOGS}
				do
					COUNT="1"
					for FULLNAME in $( find /var/tmp/portage -type f -name "${LOG}" )
					do
						FILE="$( basename "${FULLNAME}" )"

						if [ -n "$( grep "${FILE}" /root/.failures/"${SHUF}"_main )" ]
						then
							cp "${FULLNAME}" /root/.failures/"${SHUF}"_"${COUNT}"-"${LOG}"
						fi

						let COUNT++
					done
				done
			fi
		fi
		END_MERGE="$( date +%s )"
		TIME_MERGE="$( date -d@"$( expr "${END_MERGE}" - "${START_MERGE}" )" -u +%H:%M:%S )"

		if [ "$( date -d@"$( expr "${END_MERGE}" - "${START_MERGE}" )" -u +%H%M%S )" -lt "5" ]
		then
			echo ""${PACKAGE}":"${SLOT}"" >> /root/check
			emerge "${PACKAGE}":"${SLOT}" ${EOPTS} >> /root/check 2>&1
			echo -ne "\n\n\n\n" >> /root/check
		fi

		echo "$( date '+%d-%m-%Y %H:%M' ) END   ${PACKAGE}:${SLOT} (${TIME_MERGE})          ${STATUS}"
	done
}

if [ -n "${1}" ]
then
	echo "${1}" > /root/.lastcommit
fi

rm -fr /usr/portage/distfiles/*

while true
do
	env_update
	which -a git > /dev/null 2>&1 || die "Missing git"

	if [ -f /usr/portage/header.txt ]
	then
		cd "${TREE}"
		echo "Running git pull"
		git pull > /dev/null 2>&1 || die "git pull failed"
	else
		git clone https://anongit.gentoo.org/git/repo/gentoo.git "${TREE}" || die "git clone failed"
	fi

	test -f /root/.lastcommit || die "missing lastcommit file"

	echo "Running egencache"
	egencache --jobs="$( nproc )" --update --repo gentoo || die "egencache failed"

	echo "Running eix-update"
	eix-update > /dev/null 2>&1 || die "eix-update failed"

	echo "Update world"
	update_world

	INTERACTIVES="$( eix --properties-interactive --only-names )"

	cd "${TREE}"

	LAST_COMMIT="$( cat /root/.lastcommit )"
	echo "Running git rev-list"
	git rev-list "${LAST_COMMIT}"..HEAD --pretty=format:'%H %s' . | grep -v "^commit " | tac > /root/.commits
	P_TOTAL="$( cat /root/.commits | wc -l )"
	P_COUNT="0"

	unset DONE

	if [ -n "$( cat /root/.commits )" ]
	then
		echo "Running ci"
		while read LINE
		do
			cd "${TREE}"
			COMMIT="$( echo "${LINE}" | awk '{print $1}' )"
			PACKAGE="$( echo "${LINE}" | awk '{print $2}' | sed "s:\:::" )"
			TIMESTAMP="$( git show --no-patch --no-notes --pretty='%ct' "${COMMIT}" )"
			STABILIZATION="$( echo "${LINE}" | grep -iE '(stable|stabilize)' )"

			let P_COUNT++

			if [ "$( eix --only-names -e "${PACKAGE}" | wc -l )" = "1" ] && [ -d /usr/portage/"${PACKAGE}" ] && [ -z "$( echo "${INTERACTIVES}" | grep -o "${PACKAGE}" )" ] && [ -z "$( echo "${DONE}" | grep -o ""${PACKAGE}" " )" ] && [ -z "$( echo "${CI_EXCLUDE}" | grep -o ""${PACKAGE}" " )" ] && [ -z "$( echo "${PACKAGE}" | grep -E '(kde\-|\-sources|\-kernel)' )" ] && [ -z "${STABILIZATION}" ]
			then
				echo ""${P_COUNT}"/"${P_TOTAL}" PACKAGE: "${PACKAGE}"   COMMIT: "${COMMIT}"   TIMESTAMP: "$( date -d @"${TIMESTAMP}" )""
				MERGETYPE="simply"
				common_merge

				# TESTS
				if [ "${STATUS}" != "fail" ] && [ -z "$( echo "${CI_NOTEST}" | grep "${PACKAGE} " )" ] && [ -z "$( bugz -q --key "${APIKEY}" search ""${PACKAGE}" fails test" )" ] && [ "$( grep -Ro "RESTRICT=\"test\"" /usr/portage/"${PACKAGE}"/*.ebuild | wc -l )" != "$( ls /usr/portage/"${PACKAGE}"/*.ebuild | wc -l )" ]
				then
					echo ""${PACKAGE}" test" > /etc/portage/package.env
					MERGETYPE="test"
					common_merge
				fi

				# -bin packages does not need qa/symlink
#				if [[ "${PACKAGE}" != *-bin ]] && [ -z "$( bugz -q --key "${APIKEY}" search ""${PACKAGE}" calls directly" )" ]
#				then
					native-symlinks
					echo ""${PACKAGE}" qa" > /etc/portage/package.env
					MERGETYPE="qa/native-symlink"
					common_merge
#				fi

				DONE+="${PACKAGE} "

				test -f /root/ci && rm /root/ci && exit 0

				update_world
			fi

			echo "${COMMIT}" > /root/.lastcommit
		done < /root/.commits

		test -f /root/ci-end && rm /root/ci-end && exit 0
		test -f /root/ci_end && rm /root/ci_end && exit 0
	else
		/usr/local/bin/qa auto > /dev/null 2>&1

		echo "Sleeping"
		SLEEPING="yes"
		while [ "${SLEEPING}" = "yes" ]
		do
			if [ "$( git pull 2> /dev/null )" = "Already up to date." ]
			then
				sleep 120
			else
				SLEEPING="no"
			fi
		done
		echo
	fi
done