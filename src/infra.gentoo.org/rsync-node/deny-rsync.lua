function rsync_deny(applet)
  applet:send("@RSYNCD: 29.0\n")
  local banmsg = applet:get_var("txn.rsync_deny_message")
  if banmsg == nil then
    banmsg = "MESSAGE GOES HERE"
  end
  applet:send(banmsg)
  applet:send("\n")
  applet:send("@RSYNCD: EXIT\n")
end

core.register_service("deny-rsync", "tcp", rsync_deny)
