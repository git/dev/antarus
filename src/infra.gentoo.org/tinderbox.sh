#!/bin/bash

PACKAGES="$( eix --only-names )"

#SRC_TEST="yes"
SRC_TEST="no"

TOOLCHAIN="no"

die() {
	echo "${1}"
	exit 1
}

env_update() {
	env-update > /dev/null 2>&1
	source /etc/profile > /dev/null 2>&1
}

update_world() {
	echo -ne "" > /etc/portage/package.use/test
	echo -ne "" > /etc/portage/package.env
	FEATURES="-collision-protect" emerge -DuNq world > /dev/null 2>&1 || die "emerge -DuNq world failed"
	emerge -cq > /dev/null 2>&1 || die "depclean failed"

	if [ -n "$( portageq list_preserved_libs / )" ]
	then
		emerge -q1 @preserved-rebuild > /dev/null 2>&1 || die "preserved-rebuild failed"
	fi

	rm -fr /var/log/emerge.log
}

env_update

mkdir -p /root/.failures
touch /root/.list
eix-update > /dev/null 2>&1 || die "eix-update failed"

if [ -z "$( grep DATE /root/.list )" ]
then
	echo "DATE $( date )" >> /root/.list
fi

update_world
rm -fr /usr/portage/distfiles/*

for PACKAGE in ${PACKAGES}
do
	test -f /root/tinderbox && rm -fr /root/tinderbox && exit 0

	if [ -z "$( cat /root/.list | grep -w "${PACKAGE}" )" ] && [ -z "$( echo "${INTERACTIVES}" | grep -w "${PACKAGE}" )" ] && [ -z "$( echo "${TINDERBOX_EXCLUDE}" | grep -o ""${PACKAGE}" " )" ] && [ -z "$( echo "${PACKAGE}" | grep -E '(acct\-|virtual/|dev-ml|cygwin)' )" ]
	then
		env_update

		echo -ne "" > /etc/portage/package.env
		test "${SRC_TEST}" = "yes" && echo ""${PACKAGE}" test" >> /etc/portage/package.env
		test "${TOOLCHAIN}" = "yes" && echo ""${PACKAGE}" toolchain" >> /etc/portage/package.env

		SLOTS="$( V='{slotfirst}<slot>\n{}' eix --format '<availableversions::V>' -e "${PACKAGE}" )"
		if [ "${SLOTS}" = "No matches found" ]
		then
			SLOTS="0"
		fi

		for SLOT in ${SLOTS}
		do
			rm -fr /var/tmp/portage /root/.packages/"${PACKAGE}"*
			echo "$( date '+%d-%m-%Y %H:%M' ) START ${PACKAGE}:${SLOT}"
			emerge "${PACKAGE}":"${SLOT}" ${EOPTS} > /dev/null 2>&1
			if [ "${?}" != "0" ] && [ -n "$( find /var/tmp/portage -type l -name "build.log" 2>/dev/null )" ]
			then
				SHUF="$( shuf -i0000000000000000000-9999999999999999999 -n1 )"

				if [ "$( find /var/tmp/portage -type l -name "build.log" | wc -l )" -gt "1" ]
				then
					for FILE in $( find /var/tmp/portage -type l -name "build.log" )
					do
						SHUF="$( shuf -i0000000000000000000-9999999999999999999 -n1 )"
						cp "${FILE}" /root/.failures/"${SHUF}"_main
					done
				elif [ "$( find /var/tmp/portage -type l -name "build.log" | wc -l )" = "1" ]
				then
					cp "$( find /var/tmp/portage -type l -name "build.log" )" /root/.failures/"${SHUF}"_main

					for LOG in ${COMMON_ERROR_LOGS}
					do
						COUNT="1"
						for FULLNAME in $( find /var/tmp/portage -type f -name "${LOG}" )
						do
							FILE="$( basename "${FULLNAME}" )"

							if [ -n "$( grep "${FILE}" /root/.failures/"${SHUF}"_main )" ]
							then
								cp "${FULLNAME}" /root/.failures/"${SHUF}"_"${COUNT}"-"${LOG}"
							fi

							let COUNT++
						done
					done
				fi

				echo "$( date '+%d-%m-%Y %H:%M' ) END ${PACKAGE}:${SLOT}                    fail"
			else
				echo "$( date '+%d-%m-%Y %H:%M' ) END ${PACKAGE}:${SLOT}"
			fi
		done

		echo "${PACKAGE}" >> /root/.list
		update_world
	fi
done